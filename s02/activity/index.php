<?php require_once './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Activity S02</title>
    </head>
    <body>
        <h1>Divisibles of Five</h1>
        <p><?php divisibleBy5(); ?></p>

        <h1>Divisibles of Five</h1>
        <p><?php divisibleByFive(); ?></p>

        <h1>Array Manipulation</h1>
        <!-- Accept a name of the student and add it to the student array. -->
        <p><?php array_push($students, 'John Smith'); ?></p>

        <!-- Print the names added so far in the students array. -->
        <p><?php var_dump($students); ?></p>

        <!-- Count the number of names in the student array. -->
        <p><?php echo count($students); ?></p>

        <!-- Add another student then print the array and its new count. -->
        <p><?php array_push($students,'Jane Smith'); ?></p>
        <p><?php var_dump($students); ?></p>
        <p><?php echo count($students); ?></p>

        <!-- Finally, remove the first student and print the array and its count -->
        <p><?php array_shift($students); ?></p>
        <p><?php var_dump($students); ?></p>
        <p><?php echo count($students) ?></p>
        
    </body>
</html>