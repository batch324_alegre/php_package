<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s02: Selection Control Structures and Array Manipulation</title>
	</head>
	<body>
		<h1>Repetition Control Structures</h1>

		<h2>While Loop</h2>
		<p><?php whileLoop(); ?></p>
		<p><?php whileLoop1(10); ?></p>

		<h2>Do-While Loop</h2>
		<p><?php doWhileLoop(); ?></p>

		<h2>For Loop</h2>
		<p><?php forLoop(); ?></p>

		<h2>Continue and Break</h2>
		<p><?php modifiedForLoop(); ?></p>

		<h1>Array Manipulation</h2>

		<h2>Types of Arrays</h2>

		<h3>Simple Array</h3>

		<ul>
			<!-- php codes/statements can be breakdown using php tags -->
			<?php foreach($computerBrands as $brand){ ?>
				<!-- Php includes a short hand for "php echo tag" -->
				<li><?= $brand; ?></li>

			<?php }; ?>
		</ul>

		<ul>
			<?php foreach($grades as $grade){ ?>
				<li><?= $grade; ?></li>
			<?php }; ?>
		</ul>

		<ul>
			<?php foreach($tasks as $task) { ?>
				<li><?= $task; ?></li>
			<?php } ?>
		</ul>

		<h3>Associative Array</h3>
		<ul>
			<?php foreach($gradesPeriods as $period => $grade){ ?>
				<li>
					Grade in <?= $period ?> is <?= $grade ?>
				</li>
			<?php } ?>
		</ul>

		<h3>Multidimensional Array</h3>
		<ul>
			<?php
				// Each $heroes will be represented by a $team (accessing the outer array elements)
				foreach($heroes as $team){
					// Each $team will be represented by a $member(accessing the inner array elements)
					foreach($team as $member){
						?>
							<li><?php echo $member; ?></li>
						<?php
					}
				}  

			?>
		</ul>

		<ul>
			<?php
				foreach($heroes as $team){
					foreach($team as $member){
						?>
						<li><?= $member?></li>
						<?php
					}
				}
			?>
		</ul>

		<ul>
			<?php foreach($heroes as $team){foreach($team as $member){ ?>
				<li><?= $member; ?></li>
			<?php }}?>
		</ul>

		<h3>Multi-Dimensional Associative Array</h3>
		<ul>
			<?php 
				foreach($ironManPowers as $label => $powerGroup){
					foreach($powerGroup as $power){
						?>
							<li><?= "$label: $power" ?></li>
						<?php
					}
				}
			?>
		</ul>

		<ul>
			<?php foreach($ironManPowers as $powerType => $powerGroup){
				foreach($powerGroup as $power){ ?>
				<li><?= "$powerType: $power"?></li>
				<?php
				}
			}?>
		</ul>

		<h1>Array Mutators</h1>
		<h3>Sorting</h3>
		<p><?php print_r($sortedBrands); ?></p>
		<p><?php print_r($reverseSortedBranded); ?></p>

		<h3>Push</h3>
		<!-- Add element on the last part of the specified Array -->
		<?php array_push($computerBrands, 'Apple'); ?>
		<p><?php print_r($computerBrands); ?></p>

		<h3>Unshift</h3>
		<!-- Add element on the first part of the specified Array -->
		<?php array_unshift($computerBrands, 'Dell'); ?>
		<p><?php print_r($computerBrands); ?></p>

		<h3>Pop</h3>
		<!-- Delete/Remove element on the last part of the specified Array -->
		<?php array_pop($computerBrands); ?>
		<p><?php print_r($computerBrands); ?></p>

		<h3>Shift</h3>
		<!-- Delete/Remove element on the first part of the specified Array -->
		<?php array_shift($computerBrands); ?>
		<p><?php print_r($computerBrands); ?></p>

		<h3>Count</h3>
		<!-- Count the number of element in an array -->
		<p><?php echo count($computerBrands); ?></p>

		<h3>In array</h3>
		<!-- Check if the element is present in the specified array -->
		<p><?php var_dump(in_array('Acer',$computerBrands)); ?></p>

		<!-- PHP array methods https://www.w3schools.com/php/php_ref_array.asp -->




	</body>
</html>