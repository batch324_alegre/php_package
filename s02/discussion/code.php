<?php 

// [Section] Repetition Control Structures
	// It is used to execute code multiple times.

	 // While Loop
		// A While loop takes a single condition.

function whileLoop(){
	$count = 5;

	while($count !== 0){
		echo $count. '<br/>';
		$count--;
	}
};

function whileLoop1($count){

	while($count !== 0){
		echo $count. '<br/>';
		$count--;
	}
};

	// Do-While Loop
		// A do-while loop works a lot like while loop. The only difference is that do-while guarantee that the code will run/executed atleast once.

function doWhileLoop(){
	$count = 20;

	do {
		echo $count. '<br/>';
		$count--;
	} while ($count > 0);
}

	// For Loop
	/*
		for(initial value; condition; iteration){
	
		}
	*/

function forLoop(){
	for($count = 1; $count <= 20; $count++)
		echo $count. '<br>';

};

	// Continue and Break Statement
		// "Continue" is a keyword that allows the code to got the next loop without finishing the current code block
		// "Break" on the other hand is a keyword that stop the execution of the current loop.

function modifiedForLoop(){
	for ($count = 0; $count <= 20; $count++){
		// If the count is divisible by 2, do this:
		if($count % 2 === 0){
			continue;
		}
		// If not just continue the iteration
		echo $count. '<br/>';
		// If the count is greater than 10, do this:
		if($count > 10) {
			break;
		}
	}
		
};

// [Section] Array Manipulation
	// An array is a kind of variable that can hold more than one value.
	// Array are declared using array() function or square brackers'[]'.
	// In the earlier version php, we cannot used [], but as of Php 5.4 we can used the short array syntax which replace array() with [].

$studentNumbers = array('2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927');
// Before Php 5.4

$studentNumbers = ['2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927'];
// After the Php 5.4

// Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Tohshiba', 'Fujitsu'];
$tasks = [
	'drink html',
	'eat php',
	'inhale css',
	'bake javascript'

];

// Associative Array
	// Associative Array differ from numeric array in the sense that associative arrays uses descriptive names in naming the elements/values (key=>value pair).
	// Double Arrow Operator(=>) - an assignment operator that is commonly used in the creation of associative array.

$gradesPeriods = [
	'firstGrading' => 98.5,
	'secondGrading' => 94.3,
	'thirGrading' => 89.2,
	'fourthGrading' => 90.1
];
// Two-Dimensional Array
	// Two dimensional array is commonly used in image processing, good example of this is our viewing screen then uses multidimensional array of pixels.

$heroes =[
	['ironman', 'thor', 'hulk'],
	['wolverine', 'cycolps', 'jean grey'],
	['batman', 'superman', 'wonderwoman']

];

// Two-Dimensional Associative Array

$ironManPowers = [
	// 'regular' & signture is the $label
	
	'regular' => ['repulsor blast', 'rocket punch'],
	'signature' => ['unibeam']

];

// Array Iterative method: foreach();
// foreach($heroes as $hero){
// 	print_r($hero);
// }

// [Section] Array Mutators
// Array mutators modify contents of an array.

// Array Sorting

$sortedBrands = $computerBrands;
$reverseSortedBranded = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBranded);




 ?>