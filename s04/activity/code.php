<?php 
// $greet = "hello";

class Building{
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }
    public function getName(){
        return "The name of the building is $this->name.";
    }
    public function setName($name){
        $this->name = $name;
        return "The name of the building is changed to $this->name.";
    }
    public function getFloors(){
        return "The $this->name has $this->floors floors.";
    }
    public function getAddress(){
        return "The $this->name is located at $this->address.";
    }
};

class Condominium extends Building{

};

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");


?>