<?php
session_start();

$validUsername = 'johnsmith@gmail.com';
$validPassword = '1234';

$username = $_POST['username'];
$password = $_POST['password'];

if ($username === $validUsername && $password === $validPassword) {
    $_SESSION['success'] = true;
    $_SESSION['username'] = $username;

    header('Location: ./logout.php');
    exit();
} else {
    $_SESSION['error'] = 'Invalid email or password';
    header('Location: ./error.php');
    exit();
}
?>
