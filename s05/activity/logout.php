<?php
// Start the session
session_start();

if (!isset($_SESSION['success']) || $_SESSION['success'] !== true) {
    header('Location: ./login.php');
    exit();
}
?>
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Logout</title>
    </head>
    <body>
        <p>Hello, <?php echo $_SESSION['username']; ?></p>
        <form action="./login.php" method="POST">
            <Button type="submit">Logout</Button>
        </form>
    </body>
</html>