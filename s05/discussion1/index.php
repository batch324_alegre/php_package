<?php
    $tasks = ['Get Git', 'Bake HTML', 'Eat CSS', 'Drink PHP'];
    // $_GET['index'] it will get the index data from the GET request.
    // isset($_GET["index"]) will check whether the $_GET['index'] index
    if(isset($_GET["index"])){
        $indexGet = $_GET['index'];
        echo "The retrieved task from GET is $tasks[$indexGet]";
    }

    if(isset($_POST["index"])){
        $indexPost = $_POST['index'];
        echo "<br/> The retrieved task from POST is $tasks[$indexPost]";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>s05: Client-Server Communication -Discussion 1</title>
    </head>
    <body>
        <h1>Get Method</h1>
        <form method="GET">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>
        </form>

        <h1>Post Method</h1>
        <form method="POST">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">POST</button>
        </form>
        
    </body>
</html>