<!DOCTYPE html>
<html>
	<head>
		<title>S05: Client-Server Communication (Discussion)</title>
	</head>

	<body>
		<?php session_start(); ?>
		<!-- it will let us create a session that would be accessible accross our pages -->
		
		
		<h3>Add Task</h3>

		<form method="POST" action = './server.php'>

		    <input type="hidden" name="action" value="add"/>
		    Description: <input type="text" name="description" required/>
		    <button type="submit">Add</button>
		</form>

		<h3>Task Lists</h3>
		<?php if (isset($_SESSION['tasks'])) {; ?>
			<?php foreach($_SESSION['tasks'] as $index => $task){ ;?>
				
				<div>
					<form method = "POST" action = "./server.php">

						<input type="hidden" name="action" value = "update">

						<input type="hidden" name="id" value = "<?php echo $index; ?>">
						<input type="checkbox" name="isFinished" <?php echo ($task->isFinished)? 'checked' : null ;?>>
						<input type="text" name="description" value = "<?php echo $task->description; ?>">
						<button type="submit">Update</button>
                        <button type='submit'>Delete</button>

					</form>

					<form method = "POST" action = './server.php' style = 'display: inline-block;'>
						<input type="hidden" name="action" value= "delete">
						<input type="hidden" name="id" value= "<?php echo $index; ?>"> 
					</form>

				</div>


			<?php }; ?>


		<br>
		<br>
			<form method = "POST" action = './server.php'>
				<input type="hidden" name="action" value = "clear">
				<button type = 'submit'>Delete all tasks</button>
			</form>
		<?php }; ?>


	</body>
</html>
