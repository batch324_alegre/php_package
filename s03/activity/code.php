<?php 
// $greetings = "Hello";

// Parent class Person
class Person{
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName(){
        return "Your full name is $this->firstName $this->middleName  $this->lastName.";
    }
};
$person = new Person("Senku", "D.", "Ishigami");

// Child class Developer
class Developer extends Person{
    public function __construct($firstName, $middleName, $lastName) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }
    public function printName(){
        return "Your name is $this->firstName $this->middleName  $this->lastName and you are a developer.";
    }
}
$developer = new Developer("John", "Finch", "Smith");

// Child class Engineer
class Engineer extends Person{
    public function __construct($firstName, $middleName, $lastName) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }
    public function printName(){
        return "Your are an engineer named $this->firstName $this->middleName  $this->lastName.";
    }
}
$engineer = new Engineer("Harold", "Myers", "Reese");




?>