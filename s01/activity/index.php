<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Activity S01</title>
	</head>
	<body>
		<h1>Activity 1</h1>
		<h3>Full Address</h3>
		<p><?php echo getFullAddress("Philippines", "Metro Manila", "Quezon City", "3F Caswyn Bldg., Timog Avenue"); ?></p>

		<h1>Activity 2</h1>
		<h3>Letter-Based Grading</h3>
		<p><?php echo getLetterGrade(87); ?></p>
		<p><?php echo getLetterGrade(94); ?></p>
		<p><?php echo getLetterGrade(74); ?></p>


	</body>
</html>