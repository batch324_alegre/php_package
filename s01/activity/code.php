<?php

// Activity1
function getFullAddress($country, $city, $province, $specificAddress){
	return "$specificAddress, $province, $city, $country";
};

// Activity2
function getLetterGrade($grade){
	if($grade >= 98){
		return "$grade is equivalent to A+";
	}elseif($grade >= 95){
		return "$grade is equivalent to A";
	}elseif($grade >= 92){
		return "$grade is equivalent to A-";
	}elseif($grade >= 89){
		return "$grade is equivalent to B+";
	}elseif($grade >= 86){
		return "$grade is equivalent to B";
	}elseif($grade >= 83){
		return "$grade is equivalent to B-";
	}elseif($grade >= 80){
		return "$grade is equivalent to C+";
	}elseif($grade >= 77){
		return "$grade is equivalent to C";
	}elseif($grade >= 75){
		return "$grade is equivalent to C-";
	}elseif($grade <= 75){
		return "$grade is equivalent to D";
	}

}



 ?>