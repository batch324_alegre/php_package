<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics and Selection Control structure</title>
	</head>

	<body>
		<h1>Echoing Values</h1>

		<!-- using single quote for echo -->
		<p><?php echo 'Good day $name! Your given email is $email.'; ?></p>

		<!-- using double quote for echo -->
		<p><?php echo "Good day $name! Your given email is $email."; ?></p>

		<p><?php echo PI; ?></p>
		<p>This is the value of PI <?php echo PI; ?> </p>
		<p><?php echo $name;?></p>

		<h1>Data Types</h1>
		<h3>String</h3>
		<p><?php echo $state; ?></p>
		<p><?php echo $country; ?></p>
		<p><?php echo "This is my address $address"?></p>

		<h3>Integer</h3>
		<p><?php echo $age; ?> </p>
		<p><?php echo $headcount; ?></p>

		<h3>Floats</h3>
		<p><?php echo $grade;?></p>
		<p><?php echo $distanceInKilometers; ?></p>

		<h3>Boolean</h3>
		<!-- Normal echoing of boolean variables will not make it visible to the webpage. -->
		<p><?php echo var_dump($hasTravelAbroad); ?></p>
		<p><?php echo var_dump($haveSymptoms); ?></p>

		<h3>Null</h3>
		<p> <?php echo var_dump ($spouse); ?> </p>
		<p> <?php echo var_dump ($middle); ?> </p>

		<h3>Arrays</h3>
		<p><?php echo $grades[3]; ?></p>

		<h3>Objects</h3>
		<p><?php echo $gradesObj->firstGrading; ?></p>
		<p><?php echo $personObj->address->state; ?></p>
		<p><?php echo $personObj->contact[0]; ?></p>

		<h3>gettype()</h3>
		<p><?php echo gettype($state); ?></p>
		<p><?php echo gettype($age); ?></p>
		<p><?php echo gettype($grade); ?></p>
		<p><?php echo gettype($hasTravelAbroad); ?></p>
		<p><?php echo gettype($spouse); ?></p>
		<p><?php echo gettype($grades); ?></p>
		<p><?php echo gettype($gradesObj); ?></p>

		<h1>Operators</h1>

		<h3>Assignment Operator</h3>
		<p>X: <?php echo $x; ?></p>
		<p>Y: <?php echo $y; ?></p>
		<p>isLegalAge: <?php echo var_dump($isLegalAge); ?></p>
		<p>isRegistered: <?php echo var_dump($isRegistered); ?></p>

		<h3>Arithmetic Operators</h3>
		<p>Sum: <?php echo $x + $y; ?></p>
		<p>Difference: <?php echo $x - $y; ?></p>
		<p>Product: <?php echo $x * $y; ?></p>
		<p>Quotient: <?php echo $x / $y; ?></p>

		<h3>Quality Operators</h3>
		<p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
		<p>Strict Equality: <?php echo var_dump($x === '1342.14') ?></p>

		<h3>Inequality Operators</h3>
		<p>Loose Inequality: <?php echo var_dump($x != '1342.14'); ?></p>
		<p>Strict Inequality: <?php echo var_dump($x !== '1342.14'); ?></p>
		<!-- Strict Equality/Inequality is preferred so that we can check both the value and data type given -->

		<h3>Greater/Lesser Operators</h3>
		<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
		<p>Is Greater: <?php echo var_dump($x > $y); ?></p>

		<p>Is Lesser or Equal : <?php echo var_dump($x <= $y); ?></p>
		<p>Is Greater or Equal : <?php echo var_dump($x >= $y); ?></p>

		<h3>Logical Operators</h3>
		<!-- Logical operators are used to verify whether an expression or group of expression are either true or false -->

		<p>OR operator: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
		<p>AND operator: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
		<p>NOT operator: <?php echo var_dump(!$isLegalAge); ?></p>

		<h1>Function</h1>
		<p><?php echo getFullName('Gemar', 'M.', 'Alegre', 'Jr.'); ?></p>

		<h1>Selection Controls Structures</h1>
		<p><?php echo determineTyphoonIntensity(39); ?></p>

		<h1>Conditional (Ternary) Operator</h1>
		<p>78 is Under Age: <?php echo var_dump(isUnderAge(78)); ?></p>
		<p>17 is Under Age: <?php echo var_dump(isUnderAge(17)); ?></p>

		<h1>Switch Statement</h1>
		<p><?php echo determineComputerUser(5); ?></p>

		<h1>Try-Catch-Finally Statement</h1>
		<p><?php echo greetings('hello '); ?></p>




	</body>
</html>