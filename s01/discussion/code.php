<?php

// $trial = "Hello World";
// [Section] Comments
// Comments are part of the code that gets ignored by the language.
// Comments are meant to describe the algorithm of the written code.
/*
	There are two types of comments:
	-The single-line comment denote by two forward slashes(//)
	-The multi-line comment denoted by a slash or asterisk (/*)
*/

// [Section - Variables]
	// Variables are used to contain data.
	// Variables are named location for the stored value.
	// Variables are defined using the dollar ($) notation before the name of the variable.

$name = 'John Smith';
$email = 'johnsmith@gmail.com';

// [Section - Constants]
// Constants used to hold the data that are meant to be read-only.
// Constants are defined using the define() function.

	/*
		Syntax:
		define('variableName', valueOfTheVariable);
	*/

define('PI', 3.1416);

// Reassignment of a variable
$name = 'Will Smith';

// [Section - Data Types]

// Strings
$state = 'New York';
$country = 'United States of America';
$address = $state.', '.$country;

// Reassign the $address
$address = "$state, $country.";

// Integers
$age = 31;
$headcount = 26;

// floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

//Boolean
$hasTravelAbroad = false;
$haveSymptoms = true;

//Null
$spouse = null;
$middle = null;

//Array
$grades = array(98.7, 92.1, 90.2, 94.6);

//Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

// Nested Object
$personObj = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'United States of America'

	],
	'contact' => array('0912345789', '09123456780')
];

//Section - Operators
//Assignment operators(=)

// This operator is used to assign and reassign value/s of a variable
$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

//[Section] Functions
// Functions are used to make reusable codes.

function getFullName($firstName, $middleInitial, $lastName){
	// you can add here your alogorithm

	return "$lastName, $firstName, $middleInitial";
};

// [Section] Selection Control Structures
// Selection control structures are used to make code execution dynamic according to predefined conditions
	// if-elseif-else Statement

function determineTyphoonIntensity($windSpeed){
	// if-elseif-else condition
	if($windSpeed < 30) {
		return 'Not a typhoon yet.';
	}elseif ($windSpeed <= 61) {
		return 'Tropical depression detected.';
	}elseif ($windSpeed <= 88) {
		return 'Tropical storm detected.';
	}elseif ($windSpeed <= 117) {
		return 'Severe tropical storm detected.';
	}else{
		return 'Typhoon detected.';
	}
};

// Conditional (Ternary) Operator

function isUnderAge($age){

	return ($age <18) ? true : false;

};

// Switch Statement

function determineComputerUser($computerNumber){
	switch ($computerNumber) {
		case 1:
		return 'Linus Trovalds';
		break;
		case 2:
		return 'Steve Jobs';
		break;
		case 3:
		return 'Sid Meir';
		break;
		case 4:
		return 'Onel De Guzman';
		break;
		case 5:
		return 'Christian Salvador';
		break;
		default:
		return $computerNumber. ' is out of bounds.';
		break;


	}

};

// Try-Catch-Finally Statement

function greetings($str){
	try{
		// Will attempt to execute the code
		if(gettype($str) == "string"){
			// Will only run if successful
			echo $str;
		} else {
			// If not, it will return "Oops!"
			throw new Exception("Oops! ");
		}
	}
	catch (Exception $e) {
		// Catch the errors within "try"
		echo $e->getMessage();
	}
	finally{
		// Continue execution of code regardless of success and failure of code execution in 'try' block.
		echo "I did it again!";
	}
};















?>